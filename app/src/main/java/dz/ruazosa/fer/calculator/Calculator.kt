package dz.ruazosa.fer.calculator

import java.util.*

/**
 * Singleton class (cannot be instantiated more than once).
 * It contains the logic behind the calculator application.
 *
 */
object Calculator {
    private val PRIORITY_ZERO: int = 0
    private val PRIORITY_ONE: int = 1
    private var operators: Set<String> = mutableSetOf("+","*","/","-")
    private var operStack:Stack<String> = Stack<String>()
    private var operQueue:Queue<String> = ArrayDeque<String>()
    var result: Double = 0.0
    private set

    var expression: MutableList<String> = mutableListOf()
    private set

    /**
     * Sets the result to 0.0 and expression to empty.
     * Takes no arguments.
     */
    fun reset(){
        result = 0.0
        expression = mutableListOf()
    }

    /**
     * Adds number to calculator expression if possible.
     * @param number (String)
     * @throws Exception
     */
    fun addNumber(number :String){
        try{
            val num =  number.toDouble()
        } catch(e: NumberFormatException){
            throw Exception("Entered number is not valid.\n")
        }

        if(expression.count() % 2 == 0){
            expression.add(number)
        }
        else {
            throw Exception("Number and operation order is not correct.\n")
        }
    }

    /**
     * Adds an operator to the calculator expression if possible.
     * @param operator (String)
     * @throws Exception
     */
    fun addOperator(operator: String){
        if(expression.count() % 2 == 0){
            throw Exception("Number and operation order is not valid.\n")
        }
        when(operator){
            "*" -> expression.add(operator)
            "/" -> expression.add(operator)
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            else ->{
                throw Exception("Not a valid operator.\n")
            }
        }
    }


    /**
     * Calculates the expression given in infix notation and calculates the result.
     * Supports division, multiplication, addition and substraction operations.
     * Takes no parameters.
     * @throws Exception
     */
    fun evaluate(){
        if(expression.count() % 2 == 0){
            throw Exception("Expression is not valid.\n" +
                    "Check operator and number order.\n")
        }
        for(i in 0..expression.count()-1){
            if(!operators.contains(expression[i])){
                operQueue.add(expression[i])
            }
            else {
                while ((operStack.isNotEmpty()) && (checkPriority(expression[i]) <= checkPriority(operStack.peek()))) {
                    operQueue.add(operStack.pop())
                }
                operStack.push(expression[i])
            }
        }
        while(operStack.isNotEmpty()){
            operQueue.add(operStack.pop())
        }

        while(operQueue.isNotEmpty()){
            var value = operQueue.remove()
            if(!operators.contains(value)){
                operStack.push(value)
            }
            else{
                var val1 = operStack.pop()
                var val2= operStack.pop()
                when(value){
                    "+" -> operStack.push((val1.toDouble() + val2.toDouble()).toString())
                    "-" -> operStack.push((val2.toDouble() - val1.toDouble()).toString())
                    "*" -> operStack.push((val2.toDouble() * val1.toDouble()).toString())
                    "/" -> operStack.push((val2.toDouble() / val1.toDouble()).toString())
                 }
            }
        }
        result = operStack.pop().toDouble()
    }

    /**
     * Function used to determine priority level of an operator.
     * @param operator (String)
     * @return priority (Integer)
     */
    fun checkPriority(operator:String): Int{
        when(operator){
            "*"-> return PRIORITY_ONE
            "/"-> return PRIORITY_ONE
            "+"-> return PRIORITY_ZERO
            "-"-> return PRIORITY_ZERO
        } 
        return 0 //default return value
    }
}